/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * https://stackoverflow.com/questions/4179671/read-in-text-file-1-character-at-a-time-using-c (to get the function to read individual characters of the file being used).
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
bool isfSpecified = false;
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */
vector <vector<bool> > V;//vector that stores current generation
vector <vector<bool> > nextG;//vector that represents next generation
vector<bool> row;//vector for nextG

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update();
int initFromFile(const string& fname);
void mainLoop();
void dumpState(FILE* f);

char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (max_gen > 0) isfSpecified = true;
	mainLoop();
	return 0;
}
size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g) {
	size_t nbrs=0;
	for(int x = -1; x <= 1; x++){
		for(int y = -1; y <= 1; y++){
			if(x == 0 && y == 0){
				continue;
			}
			int tempRow = i + x;
			int tempCol = j + y;
			if(tempRow < 0){
				tempRow = g.size();
			} else if(tempRow > g.size()){
				tempRow = 0;
			}
			if(tempCol < 0){
				tempCol = g[0].size();
			} else if(tempCol > g[0].size()){
				tempCol = 0;
			}
			if(g[tempRow][tempCol] == true){
				nbrs++;
			}
		}
	}

 	return nbrs;
}

/* updating the vector that represents the next generation of cells */
void update() {
	for(size_t i=0; i < V.size();i++) {
		for(size_t j=0; j < V[i].size();j++) {
			if(nbrCount(i,j,V) < 2 || nbrCount(i,j,V) > 3) row.push_back(false);
			else row.push_back(true);
		} 	
	for(size_t i=0; i<V.size();i++) {
		for(size_t j=0; j<V[i].size();j++) {
			if(nbrCount(i,j,V)==3)row.push_back(true);
			else row.push_back(false);
		}
		nextG.push_back(row);
		row.clear();
	}
	V=nextG;
	nextG.clear();
	}
}
void mainLoop() {
	/* update, write, sleep */
	initFromFile(initfilename);
	update();
	fworld=fopen(wfilename.c_str(),"wb");
	dumpState(fworld);
	if(max_gen > 0 || isfSpecified == false) mainLoop();
}

int initFromFile(const string& fname) {
	FILE* f = fopen(fname.c_str(), "rb");
	if (!f) exit(1);
	int read;
	size_t row = 0, column = 0;
	while ((read = getc(f)) != EOF) {
		column++;
		if (read == '\n') row++;
	}
	column = (column/row) - 1;
	rewind(f);
	vector<bool> temp;
	while ((read = getc(f)) != EOF) {
		if (read == '.') temp.push_back(false);
		if (read == 'O') temp.push_back(true);
		if (read == '\n') {
			V.push_back(temp);
			temp.clear();
		}
	}
	rewind(f);
	
	if (max_gen == 0) {
		for (size_t i = 0; i < V.size(); i++) {
			for (size_t j = 0; j < V[i].size(); j++) {
				if (V[i][j] == false) printf(".");
				else if (V[i][j] == true) printf("O");
			}
			printf("\n");
		}
	}

//	fread(c, 1, 8000, f);
	fclose(f);
	return 0;
}

void dumpState(FILE* f) {
	char c;
	for(size_t i=0; i<V.size();i++) {
		for(size_t j=0; j<V[i].size();j++) {
			if(V[i][j]) c='O';
		//	else c='.';
			fwrite(&c,1,1,f);
		}
		c='\n';
		fwrite(&c,1,1,f);
	}
	fclose(f);
}
